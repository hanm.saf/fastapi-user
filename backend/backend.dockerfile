#FROM tiangolo/uvicorn-gunicorn-fastapi:python3.7
FROM python:3.6-buster AS builder

#RUN pip install pyinstaller && pip install --upgrade 'setuptools<45.0.0'
#
#COPY app/req.txt /usr/src/app/
#RUN pip install --no-cache-dir -r req.txt

#COPY ./requirements.txt /workspace/requirements.txt
#RUN pip install -r /workspace/requirements.txt
#
#COPY src  /workspace
#RUN cd /workspace && pip install -e .
#RUN cd /workspace && mkdir -p dist && pyinstaller --onefile main.py -n plugin
#RUN ldd /workspace/dist/plugin


WORKDIR /app/
COPY ./app/pyproject.toml  /app/

# Install Poetry
RUN curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | POETRY_HOME=/opt/poetry python && \
    cd /usr/local/bin && \
    ln -s /opt/poetry/bin/poetry && \
    poetry config virtualenvs.create false

ENV PATH="/root/.poetry/bin:$PATH"

RUN which poetry

#Change with run pipinstaller

# Copy poetry.lock* in case it doesn't exist in the repo

# Allow installing dev dependencies to run tests
ARG INSTALL_DEV=false
RUN bash -c "if [ $INSTALL_DEV == 'true' ] ; then poetry install --no-root ; else poetry install --no-root --no-dev ; fi"

# For development, Jupyter remote kernel, Hydrogen
# Using inside the container:
# jupyter lab --ip=0.0.0.0 --allow-root --NotebookApp.custom_display_url=http://127.0.0.1:8888
ARG INSTALL_JUPYTER=false
RUN bash -c "if [ $INSTALL_JUPYTER == 'true' ] ; then pip install jupyterlab ; fi"

COPY ./app /app
ENV PYTHONPATH=/app
